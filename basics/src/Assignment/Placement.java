package Assignment;
import java.util.Scanner;



public class Placement
{



   public static void main(String[] args)
    {
        int cse;
        int ece;
        int mech;
        
        try
        {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the Number of Student Placed in CSE: ");
            cse= sc.nextInt();
            System.out.println("Enter the number of Students Placed in ECE: ");
            ece= sc.nextInt();
            System.out.println("Enter the number of students place in Mech: ");
            mech= sc.nextInt();
            
        }
        catch (Exception e)
        {
            System.out.println("Invalid Input");
            return;
        }
        if(cse<0||ece<0||mech<0)
        {
            System.out.println("Invalid Input,Please only Enter Non Negative Integers");
            return;
        }
        if(cse== ece && ece==mech )
        {
            System.out.println("None of the department have got the highest Placement");
            return;
        }
        System.out.println("Highest Placement from SRV for the Year 2021-22 goes to :");
        
        if(cse==ece && cse>mech)
        {
            System.out.println("CSE DEPT");
            System.out.println("ECE DEPT");
        }
        if(cse==mech && cse>ece)
        {
            System.out.println("CSE DEPT");
            System.out.println("MECH DEPT");
        }
        if(ece==mech && ece>cse)
        {
            System.out.println("ECE DEPT");
            System.out.println("MECH DEPT");
        }
        
        else if(cse>ece && cse>mech)
        {
            System.out.println("CSE DEPT");
        }
        else if (ece>mech)
        {
            System.out.println("ECE DEPT");
        }
        else
        {
            System.out.println("MECH DEPT");
        }
    }



}